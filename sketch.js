class WebMidiController {
  constuctor() {
    if (!navigator.requestMIDIAccess) {
      console.log("Web MIDI not supported in this browser")
    }
  }

  getMidiAccess(noteMap) {
    navigator.requestMIDIAccess().then((midiAccess) => {
      console.log("MIDI ready!");

      let inputs = midiAccess.inputs.values()
      for (let input = inputs.next(); input && !input.done; input = inputs.next()) {
        input.value.onmidimessage = (message) => this.onMIDIMessage(message, noteMap)
      }

    }, (msg) => {
      console.log("Failed to get MIDI access - " + msg);
    });
  }

  onMIDIMessage(message, noteMap) {
    let command = message.data[0]
    let note = message.data[1]
    let velocity = message.data[2]
    if (noteMap[note]) {
      noteMap[note](command, note, velocity)
    }
  
    // console.log(`Raw MIDI message data: ${message.data}`)
  }
}

// SETTINGS
p5.disableFriendlyErrors = true;

// Notemap function definitions
const controlRed = (command, note, velocity) => {
  globalR = velocity * 1.969
}

const controlGreen = (command, note, velocity) => {
  globalG = velocity * 1.969
}

const controlBlue = (command, note, velocity) => {
  globalB = velocity * 1.969
}

const drawSquare = (command, note, velocity) => {
  if (command === 153) {
    SHAPES.push(new Shape(velocity / 12.7))
  }
}

const noteMap = {
  18: controlRed,
  19: controlGreen,
  16: controlBlue,
  36: drawSquare
}

// Initialize web midi controller
new WebMidiController().getMidiAccess(noteMap)

// Global variables
let globalR = 0
let globalG = 0
let globalB = 0

let SHAPES = []


// Shape class to allow multiple squares at the same time
class Shape {

  constructor(strokeSize) {
    this.strokeSize = strokeSize
    this.size = 200
  }

  drawShape() {
    strokeWeight(this.strokeSize);
    stroke(0);
    fill(0, 0, 0, 0)
    square(width / 2, height / 2, this.size)
  }
}


// p5.js functions
function setup() {
  frameRate(60)
  rectMode(CENTER)
  createCanvas(600, 600);
}

function draw() {
  background(globalR, globalG, globalB);

  SHAPES.forEach(shape => {
    shape.size += 6
    if (shape.size < 600) {
      shape.drawShape()
    }
  })

  if (SHAPES.length > 6) {
    SHAPES.shift()
  }

}