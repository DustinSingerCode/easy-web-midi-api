# README #

This is a p5.js sketch to demonstrate a generic way to setup the event listeners for the web midi api.

You first want to setup what I call a note map wich will be a javascript object that contains multiple key value pairs, where key is the note id that comes from the midi message, and value is a function that should be executed when the note is triggered. All functions must take at least 3 arguments (command, note, velocity).

__Example:__

```
function testFunction(command, note, velocity) {
	console.log(command, note, velocity)
}

const noteMap = {
	36: testFunction
}
```

Next you want to initialize the WebMidiController class and call the getMidiAccess function and passing the noteMap to it.

__Example:__

```
new WebMidiController().getMidiAccess(noteMap)
```

That's it! Your midi controller should now be able to control your p5 sketches in real-time.

